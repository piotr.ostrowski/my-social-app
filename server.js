const express = require('express');
const fs = require('fs');
const bcrypt = require('bcrypt');

const saltRounds = 10;

const app = express();
app.use(express.json())
const users = JSON.parse(fs.readFileSync('./api/users.json'));
const posts = JSON.parse(fs.readFileSync('./api/posts.json'));

app.get('/api/users', (req, res) => {
    res.json(users)
});

app.get('/api/posts', (req, res) => {
    res.json(posts)
});

app.post('/api/register', (req, res) => {
    users.push(req.body);
    fs.writeFile('./api/users.json', JSON.stringify(users, null, 2), () => console.log('dodałem uzytkownik'));
    res.json(users);
});

app.post('/api/addpost', (req, res) => {
    posts.push(req.body);
    fs.writeFile('./api/posts.json', JSON.stringify(posts, null, 2), () => console.log('dodałem post'));
    res.json(posts);
});

app.post('/api/deletepost', (req, res) => {
    const newPosts = posts.filter(post => post.id !== req.body.id);
    fs.writeFile('./api/posts.json', JSON.stringify(newPosts, null, 2), () => console.log('usunalem post'));
    res.json(newPosts);
})

const PORT = 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));