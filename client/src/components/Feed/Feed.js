import React, { useEffect, useState } from 'react';
import "./Feed.css"
import { Formik, Field } from 'formik';
import PostsFilter from './PostsFilter/PostsFilter';



const Feed = (props) => {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    const yyyy = today.getFullYear();

    const todayString = mm + '/' + dd + '/' + yyyy;

    const [posts, setPosts] = useState([])
    const [postsToMap, setPostsToMap] = useState([])

    const addPost = (values) => {
        const randomizer = () => Math.floor(10000 * Math.random())
        const getId = (r) => {
            return (posts.some(post => post.id === r)) ? getId(randomizer()) : r;
        }
        const data = {
            id: getId(randomizer()),
            user: props.user,
            text: values.text,
            date: todayString
        };
        const options = {
            method: 'POST',
            headers: {
                "Content-Type": 'application/json'
            },
            body: JSON.stringify(data)  
        };
        fetch('/api/addpost', options)
            .then(res => res.json())
            .then(data => {
                setPosts(data)
                setPostsToMap(data)
            })
            .catch(err => console.log(err))
    };

    const deletePost = (postId) => {
        const data = {
            id: postId
        };
        const options = {
            method: 'POST',
            headers: {
                "Content-Type": 'application/json'
            },
            body: JSON.stringify(data)  
        };
        fetch('/api/deletepost', options)
            .then(res => res.json())
            .then(data => {
                setPosts(data)
                setPostsToMap(data)
            })
            .catch(err => console.log(err))

    }

    const deleteButton = (username, postId) => {
        return (username) ? (<button type="button" onClick={() => deletePost(postId)} className="btn btn-outline-danger inpost">Delete</button>) : <></>;
    }

    useEffect( () => {
        fetch('/api/posts')
            .then(res => res.json())
            .then(data => {
                setPosts(data)
                setPostsToMap(data)
            })
    }, [])
    
    if (posts.length === 0) return (<h1>Loading...</h1>)
    return (
        <div className="feed-container">
            <div className="postform-container">
                <Formik 
                    initialValues={{text: ""}}
                    onSubmit={addPost}>
                    
                    {(formProps) => (
                        <form onSubmit={formProps.handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="exampleFormControlTextarea1" id="postform-label">Post something</label>
                                <Field name={"text"} component="textarea" className="form-control" id="exampleFormControlTextarea1" rows="3" />
                            </div>
                            <div className="postform-button-holder">
                                <button type="submit" className="btn btn-outline-success">Submit</button>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleFormControlFile1">With an image...</label>
                                <input type="file" className="form-control-file" id="exampleFormControlFile1" />

                            </div>
                        </form>
                    )}

                </Formik>
            
                <div className="posts-filter-container">
                    <PostsFilter handleData={setPostsToMap} data={posts} />
                </div>

            </div>
            
            <div className="postlist-container">
                {postsToMap.map( (post) => {return (
                    <div className="card" style={{width: '36rem'}}>
                        <div className="card-body">
                            <h5 className="card-title">{post.user}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">{post.date}</h6>
                            <p className="card-text">{post.text}</p>
                            
                        </div>
                    </div>
                    )})}

            </div>
        </div>
    )
};

export default Feed;