import React, { useEffect, useState } from "react";


const PostsFilter = (props) => {
    const [filters, setFilters] = useState({
                                        username: "",
                                        text: "",
                                        })

    useEffect(() => {
        props.handleData(props.data.filter(
            post => {
                const byUsername = (filters.username === "") ? true : post.user.includes(filters.username);
                const byContent = (filters.content === "") ? true : post.text.includes(filters.text);
                return byUsername && byContent;               
            }
        ))
    }, [filters])

    // const handleDateSort = (e) => {
    //     props.handleData(props.data2.reverse())
    // }

    return (<>
        <center><h5>Filter</h5></center>

        <label htmlFor="formGroupExampleInput">By username</label>
        <input type="text" onChange={(e) => setFilters({...filters, username: e.target.value})} className="form-control" id="formGroupExampleInput" />

        <label htmlFor="formGroupExampleInput">By content</label>
        <input type="text" onChange={(e) => setFilters({...filters, text: e.target.value})} className="form-control" id="formGroupExampleInput" />

        {/* <div class="form-check">
            <input className="form-check-input" type="checkbox" id="defaultCheck1" onClick={handleDateSort}/>
            <label className="form-check-label" htmlFor="defaultCheck1">
                By date ⬆️
            </label>
        </div> */}

    </>)
}

export default PostsFilter;