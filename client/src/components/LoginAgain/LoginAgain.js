import React, { useState } from 'react';
import './LoginAgain.css'
import { Link } from 'react-router-dom';


const LoginAgain = (props) => {
    
    return (
        <div className="login-again">
            <h1>Logged out</h1>
            <Link to="/" >Back to the login page</Link>
        </div>
    )
}
    
export default LoginAgain;