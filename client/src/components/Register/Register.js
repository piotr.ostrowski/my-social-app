import React, { useState, useEffect } from 'react';
import './Register.css'
import { useHistory } from 'react-router-dom'
import 'formik'
import { Formik, Field } from 'formik';

const Register = (props) => {
    const history = useHistory();
    const [usersList, setUsersList] = useState();
    const [alert, setAlert] = useState();

    useEffect( () => {
        fetch('/api/users')
            .then(res => res.json())
            .then(data => setUsersList(data))
    }, [])

    const validateForm = (values) => {
        if (values.username.length < 4 || values.username.length > 15) {
            setAlert((
                <div id="login-error-alert" className="alert alert-warning" role="alert">
                    Username must be 4-15 characters!
                </div>
            ));
            setTimeout(() => {
                setAlert((<></>))}, 2000);
            return false;
        } else if (values.password.length < 5) {
            setAlert((
                <div id="login-error-alert" className="alert alert-warning" role="alert">
                    Password must be min. 5 characters!
                </div>
            ));
            setTimeout(() => {
                setAlert((<></>))}, 2000);
            return false;
        } else if (values.password !== values.passwordRepeat) {
            setAlert((
                <div id="login-error-alert" className="alert alert-warning" role="alert">
                    Passwords do not match!
                </div>
            ));
            setTimeout(() => {
                setAlert((<></>))}, 2000);
            return false;
        } else {
            setAlert((
                <div id="login-error-alert" className="alert alert-success" role="alert">
                    Registration succeed!
                </div>
            ));
            setTimeout(() => {
                setAlert((<></>))}, 2000);

            return true;
        };
    };

    const handleRegister = (values) => {
        const exists = usersList.some(user => user.username === values.username)
        if (exists) {
            setAlert((
                <div id="login-error-alert" className="alert alert-warning" role="alert">
                    User already exists!
                </div>
            ))
            setTimeout(() => {
                setAlert((<></>))}, 2000)
        } else if (validateForm(values)) {
            const randomizer = () => Math.floor(10000 * Math.random())
            const getId = (r) => {
                return (usersList.some(user => user.id === r)) ? getId(randomizer()) : r;
            }

            const data = {
                id: getId(randomizer()),
                username: values.username,
                password: values.password
            };
            const options = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            };
            
            fetch('/api/register', options)
                .then(res => res.json())
                .then(data => console.log(data))
                .catch(err => console.log(err))
        };
    };

    const goToLogin = () => {
        history.push("/")
    }

    return (<div className="login-card">
        <h2>Register</h2>
        <Formik 
            initialValues = {{
                username: "",
                password: "",
                passwordRepeat: ""
            }} 
            onSubmit={handleRegister} >

            {(formProps) => (
                <form onSubmit={formProps.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Username</label>
                        <Field name={"username"} placeholder="Must be 4-15 characters . . ." type="text" className="form-control" id="formGroupExampleInput" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupExampleInput2">Password</label>
                        <Field name={"password"} placeholder="Min. 5 characters . . ." type="password" className="form-control" id="formGroupExampleInput2" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupExampleInput2">Password</label>
                        <Field name={"passwordRepeat"} type="password" className="form-control" id="formGroupExampleInput3" />
                    </div>
                    <button onClick={goToLogin} id="login-button" type="button" className="btn btn-outline-success">Log in</button>
                    <button id="register-button" type="submit" className="btn btn-outline-danger">Register</button>
                    {alert}
                </form>
            )}
        </Formik>

    </div>)
}

export default Register;