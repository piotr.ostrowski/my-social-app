import React, { useState } from 'react';
import NavBar from '../NavBar/NavBar';
import { BrowserRouter as Router, Route, Switch, useHistory } from 'react-router-dom';
import Feed from '../Feed/Feed';
import './Home.css'
import Radio from '../Radio/Radio';


const Home = (props) => {
    const username = props.user
    const history = useHistory();
    console.log(props.user)
    if (props.user === "") {
        history.push('/login-again')
    }
    return (
        <div className="home-container">
            <NavBar user={username} />
            <div className="content-container">
            <Switch>
                <Route path="/app/feed" exact component={() => (<Feed user={username}/>)}/>
                <Route path="/app/radio" exact component={Radio} />
            </Switch>
            </div>

        </div>
    )
}
    
export default Home;