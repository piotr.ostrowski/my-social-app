import React from 'react';
import './NavBar.css'
import { Link, useHistory } from 'react-router-dom';

const NavBar = (props) => {

    const history = useHistory();

    return (
        <nav className="navbar navbar-expand-md navbar-light fixed-top">
            <h2>Welcome, {props.user}</h2>
            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link to="/app/feed" className="nav-link">Feed</Link>
                </li>
                <li className="nav-item">
                    <Link to="/app" className="nav-link">User Page</Link>
                </li>
                <li className="nav-item">
                    <Link to="/app" className="nav-link">Radio</Link>
                </li>
                <li className="nav-item">
                    <Link to="/" className="nav-link" id="log-out-link">Log out</Link>
                </li>
            </ul>

        </nav>
    )
}
export default NavBar;