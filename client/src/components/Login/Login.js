import React, { useState, useEffect } from 'react';
import './Login.css'
import { useHistory } from 'react-router-dom'
import 'formik'
import { Formik, Field } from 'formik';

const Login = (props) => {
    const history = useHistory();
    const [usersList, setUsersList] = useState();
    const [alert, setAlert] = useState();

    useEffect( () => {
        fetch('/api/users')
            .then(res => res.json())
            .then(data => setUsersList(data))
    }, [])


    const handleLogin = (values) => {
        const password = usersList.reduce((acc, user) => {
            if (user.username === values.username) {
                acc = user.password
            }
            return acc;
        }, "")

        if (password.length !== 0) {
            if (password === values.password) {
                props.loginUser(values.username)
                history.push("/app")
            } else {
                setAlert((
                    <div id="login-error-alert" className="alert alert-danger" role="alert">
                        Wrong password!
                    </div>
                ))
                setTimeout(() => {
                    setAlert((<></>))}, 2000)
            };
        } else {
            setAlert((
                <div id="login-error-alert" className="alert alert-danger" role="alert">
                    User not found!
                </div>
            ))
            setTimeout(() => {
                setAlert((<></>))}, 2000)
        }
    }

    const goToRegister = () => {
        history.push('/register')
    }

    return (<div className="login-card">
        <h2>Login</h2>
        <Formik 
            initialValues = {{
                username: "",
                password: ""
            }} 
            onSubmit={handleLogin} >

            {(formProps) => (
                <form onSubmit={formProps.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Username</label>
                        <Field name={"username"} type="text" className="form-control" id="formGroupExampleInput" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupExampleInput2">Password</label>
                        <Field name={"password"} type="password" className="form-control" id="formGroupExampleInput2" />
                    </div>
                    <button id="login-button" type="submit" className="btn btn-outline-success">Log in</button>
                    <button onClick={goToRegister} id="register-button" type="button" className="btn btn-outline-danger">Register</button>
                    {alert}
                </form>
            )}
        </Formik>

    </div>)
}

export default Login;