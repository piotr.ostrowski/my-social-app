import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import Login from './components/Login/Login'
import Register from './components/Register/Register';
import Home from './components/Home/Home';
import Feed from './components/Feed/Feed';
import LoginAgain from './components/LoginAgain/LoginAgain';

function App() {

  const [user, setUser] = useState("");

  console.log('app loaded')
  return (
    <BrowserRouter>
        <Route path="/" exact component={() => (<Login loginUser={setUser} />)} />
        <Route path="/app" component={() => (<Home user={user} />)} />
        <Route path="/register" exact component={Register} />
        <Route path="/login-again" exact component={LoginAgain} />
    </BrowserRouter>
  );
}

export default App;
